lang = {
    NAME_CAN_T_BLANK: 'Введите имя',
    PHONE_CAN_T_BLANK: 'Введите телефон',
    SUBJECT_CAN_T_BLANK: 'Введите тему',
    EMAIL_CAN_T_BLANK: 'Введите email',
    EMAIL_NOT_VALID: 'Введите корректный email'
};