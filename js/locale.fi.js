lang = {
    NAME_CAN_T_BLANK: 'Anna nimi',
    PHONE_CAN_T_BLANK: 'Anna puhelimen',
    SUBJECT_CAN_T_BLANK: 'Kirjoita aihe',
    EMAIL_CAN_T_BLANK: 'Anna sähköpostiosoite',
    EMAIL_NOT_VALID: 'Kirjoita voimassa oleva sähköpostiosoite'
};